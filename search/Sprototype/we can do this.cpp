#include <iostream>
#include <string>
#include <map>
#include<sstream>
#include <vector>
#include <iterator>
#include <cmath>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include<sstream>
#include <map>
#include <vector>
#include <boost/serialization/map.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 
#include <boost/serialization/list.hpp> 
#include <boost/serialization/vector.hpp> 
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

/*
Include Boost
*/

using namespace std;

//prototyping
void split(vector<string> &terms, string search);
void printResults(vector<pair<int, string>>docs, map<string, map<string, vector<int>>> Doc, map<string, vector<int>> index, string search);




void split(vector<string> &terms, string search) {
	/**function which splits search term and stores it in vector.
	* it takes input, a vector to store the search terms in and
	* the search string the user enters.
	* it outputs, vector which has the search string split into token by whitespace */
	terms.clear();
	stringstream ss(search);
	string word;
	while (getline(ss, word, ' ')) {
		terms.push_back(word);
	}

}

void DeserializeMaps(map<string, vector<string>> &FI, map<string, vector<string>> &II) {
	/**function to load maps, using boost
	* it takes input, the map to store Forward Index and
	* map to store Inverted Index.
	*  it then, loads Inverted index and forward index into their respective maps */

	//serializied map file should be in default folder
	ifstream maps("forward.map", ios::binary);
	if (!maps.is_open()) {
		cerr << "Serialized forward map not Found" << endl;
		return;
	}
	boost::archive::binary_iarchive oarch(maps);
	oarch >> FI;
	maps.close();

	ifstream maps2("inverted.map", ios::binary);
	if (!maps2.is_open()) {
		cerr << "Serialized inverted map not found" << endl;
		return;
	}
	boost::archive::binary_iarchive oarch1(maps2);
	oarch1 >> II;
	maps2.close();
}

void Process(map<string, vector<string>> &FI, map<string, vector<string>> &II, vector<string> terms, string search) {
	/**function which compiles the results into a map based on the search terms based on frequency ranking
	* it takes input, the Forward Index map,
	* the Inverted Index map,
	* the vector of terms
	* and the search string.
	**/
	//searching
	vector<int> empty;
	map<string, vector<int>> index; // holds words and their index in a particular document
	map<string, map<string, vector<int>>> Doc; // holds document and occurences of each term in it.
	vector<pair<int, string>>docs;
	string tempStr = terms[0];
	string tempDoc;
	//vector of vectors that contain documents to be searched for each term entered
	vector<vector<string>> articles;
	for (int i = 0; i < terms.size(); i++) {
		articles.push_back(II[terms[i]]);
	}
	//creates a map with document title as key and search terms with their indexes in that document
	for (int j = 0; j < articles.size(); j++) {
		for (int m = 0; m < articles[j].size(); m++) {
			tempDoc = articles[j][m];
			for (int i = 0; i < terms.size(); i++) {
				tempStr = terms[i];
				int count = 0;//index
				for (int k = 0; k < FI[tempDoc].size(); k++) {
					if (FI[tempDoc][k].compare(tempStr) == 0) {
						empty.push_back(count);
					}
					count++;

				}
				index[tempStr] = empty;
				empty.clear();
				Doc[tempDoc] = index;
			}

		}

	}
	//calculate word frequency and forms pairs.
	string s;
	int freq = 0;
	for (map<string, map<string, vector<int>>>::iterator it = Doc.begin(); it != Doc.end(); it++) {
		s = it->first;
		for (map<string, vector<int>>::iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {
			freq += it2->second.size();
		}
		docs.push_back(make_pair(freq, s));
		freq = 0;
	}
	//sorts doc according to word index. Page Rank.
	sort(docs.rbegin(), docs.rend());
	//print result function call
	printResults(docs, Doc, index, search);
}

void printResults(vector<pair<int, string>>docs, map<string, map<string, vector<int>>> Doc, map<string, vector<int>> index, string search) {
	/**function which prints the results of the search query
	* it takes input, Forward Index map,
	* the Inverted Index map,
	* the vector of terms,
	* and the search string.
	*  it then print results
	**/
	int i = 0;
	bool notFound = true;
	if (!index[search].empty()) {
		notFound = false;
		cout << "Document Title: " << search << endl;
		cout << "Search Terms are title of the document\n";

	}

	//printing the results
	while (i < docs.size()) {
		for (map<string, map<string, vector<int>>>::iterator it = Doc.begin(); it != Doc.end(); it++) {
			if (docs[i].second == it->first) {
				i++;

				cout << "\nDocument Title: " << it->first << "\n" << "Search Term: " << endl;
				for (map<string, vector<int>>::iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {

					if (!it2->second.empty()) {
						notFound = false;

						cout << it2->first << " occurs at indices: ";

						for (int m = 0; m < it2->second.size(); m++) {
							cout << it2->second[m] << " ";
						}

						cout << "\n";
					}
				}

				cout << "\n\n";
			}
		}

	}

	if (notFound == true) {
		cout << "No matching documents found." << endl;
	}
}

int main() {
	/**main function that takes input from user
	* that is the string to be searced
	**/
	//Forward Index and Reverse Index Maps
	map<string, vector<string>> FI;
	map<string, vector<string>> II;

	//input string
	string search;

	// holds search terms
	vector<string> terms;

	//Deserializing maps to form II and FI
	DeserializeMaps(FI, II);

	//Multi term Searching
	cout << "Enter search terms: ";
	getline(cin, search);
	transform(search.begin(), search.end(), search.begin(), ::tolower); //converts string to lowercase

																		//split terms and store them in vector terms
	split(terms, search);

	//Initialize the searching process
	Process(FI, II, terms, search);

	system("pause");
	return 0;
}